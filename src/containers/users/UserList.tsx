import axios from 'axios';
import React from 'react'
import User from './interfaces/User'
import UserApiResponse from './interfaces/UserApiResponse';
import UserCard from './UserCard';

interface UserListProps {
    
}

const UserList: React.FC<UserListProps> = (props) => {
    const [apiResponse, setApiResponse] = React.useState<UserApiResponse | undefined>();
    const [users, setUsers] = React.useState<User[]>([]);

    React.useEffect(() => {
        const USER_API_ENDPOINT = `https://reqres.in/api/users/`
        axios
            .get(USER_API_ENDPOINT)
            .then(response => response.data)
            .then((userApiResponse: UserApiResponse) => {
                setApiResponse(userApiResponse)
                setUsers(userApiResponse.data)
            })

    }, [])

    return <div className="container">
        {/* <pre>{JSON.stringify(apiResponse, null, 4)}</pre> */}
        <div className="row">
            {users.map((user, idx) => <UserCard user={user} key={idx} />)}
        </div>
    </div>
}

export default UserList
