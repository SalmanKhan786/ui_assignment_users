import User from "./User";

export default interface UserApiResponse {
    data: User[];
    page: number;
    per_page: number;
    total: number;
    total_pages: number;
}