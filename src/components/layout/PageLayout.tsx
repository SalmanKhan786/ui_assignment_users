import styled from '@emotion/styled'
import React from 'react'
import PageFooter from './PageFooter'
import PageHeader from './PageHeader'

interface PageLayoutProps {
    
}

const PageLayout: React.FC<PageLayoutProps> = (props) => {
    return <PageLayoutUi className="ui__page">
        <PageHeader />
        <div className="ui__page__content">
            {props.children}
        </div>
        <PageFooter />
    </PageLayoutUi>
}

const PageLayoutUi = styled.section`
    display: flex;
    flex-direction: column;
    height: 100vh;
    overflow: hidden;

    .ui__page__content {
        flex:1;
        height: 100%;
        overflow-y: auto;
    }

`

export default PageLayout
