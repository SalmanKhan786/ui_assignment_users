import styled from '@emotion/styled'
import React from 'react'

interface PageHeaderProps {

}

const PageHeader: React.FC<PageHeaderProps> = (props) => {
    return <HeaderUiView className="p-3 bg-primary text-white">
        <h3>User List</h3>
    </HeaderUiView>
}

const HeaderUiView = styled.header`
    
`

export default PageHeader