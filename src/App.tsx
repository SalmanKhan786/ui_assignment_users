import React from 'react';
import './App.css';
import PageLayout from './components/layout/PageLayout';
import "bootstrap/dist/css/bootstrap.min.css"
import UserList from './containers/users/UserList';

function App() {
  return <PageLayout>
    <UserList />
  </PageLayout>
}

export default App;
